//
//  AppDelegate.swift
//  AppTargetiOS
//
//  Created by Andres Beltran on 11/10/18.
//  Copyright © 2018 Andres Beltran. All rights reserved.
//

import UIKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        let mocaStarted = MOCA.initializeSDK()

        if !mocaStarted {
            NSLog("Cannot initialize MOCA SDK")
        }
        
        if launchOptions != nil {
            // Launched from push notification
            let notification = launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? [AnyHashable: Any] ?? [AnyHashable: Any]()
            if !notification.isEmpty {
                // Notify MOCA the app started from push
                MOCA.handleRemoteNotification(notification)
            }
        }
        let currentInstance: MOCAInstance? = MOCA.currentInstance()
        if currentInstance != nil {
            currentInstance?.setValue("property_1_value", forProperty: "guest_property_1")
            currentInstance?.setValue("property_2_value", forProperty: "guest_property_1")
            currentInstance?.setValue(2.9, forProperty: "guest_property_3")
            currentInstance?.save({ (instance: MOCAInstance?, error: Error?) in
                if (error != nil) {
                    print("Save instance failed: (%@)", error ?? "Error not available")
                }
            })
        }
        if launchOptions?[UIApplicationLaunchOptionsKey.location] != nil {
            return true
        }
        
         return false
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    // The method will be called on the delegate only if the application is in the foreground. If the method is not implemented or the handler is not called in a timely manner then the notification will not be presented. The application can choose to have the notification presented as a sound, badge, alert and/or in the notification list. This decision should be based on whether the information in the notification is otherwise visible to the user.
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        if(MOCA.initialized() && MOCA.isMocaNotification(notification)) {
            MOCA.userNotificationCenter(center, willPresent: notification, withCompletionHandler: completionHandler)
        }
    }
    
    // The method will be called on the delegate when the user responded to the notification by opening the application, dismissing the notification or choosing a UNNotificationAction. The delegate must be set before the application returns from application:didFinishLaunchingWithOptions:
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if(MOCA.initialized() && MOCA.isMocaNotification(response)) {
            MOCA.userNotificationCenter(center, didReceive: response, withCompletionHandler: completionHandler)
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        print("deviceToken: @%", deviceToken)
        if MOCA.initialized() {
            MOCA.registerDeviceToken(deviceToken)
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("remote push notifications registration failed")
    }

}


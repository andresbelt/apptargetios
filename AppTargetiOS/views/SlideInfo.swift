//
//  SlideInfo.swift
//  AppTargetiOS
//
//  Created by Andres Beltran on 11/10/18.
//  Copyright © 2018 Andres Beltran. All rights reserved.
//

import UIKit

class SlideInfo: UIView {
    
    @IBOutlet weak var labelTitle: UITextView!
    @IBOutlet weak var labelDesc: UITextView!
    @IBOutlet weak var imageVIew: UIImageView!
    
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
